package org.cpu.grinder;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Richard on 1/21/14.
 */
public class LoaderTest {

    @Test
    public void lengthOfTime(){
        long lengthTime = 5000;

        Loader testLoader = new Loader(lengthTime);
        long start = System.currentTimeMillis();

        testLoader.fib();


        long end = System.currentTimeMillis();
        long totalTime = end - start;

        Assert.assertTrue(totalTime >= lengthTime);


    }

}
