package org.cpu.grinder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richard on 8/26/14.
 */
public class FibonacciCalc {

    protected static List<BigInteger> fibCache = new ArrayList<BigInteger>();

    private int nthFibonacci = 0;

    FibonacciCalc(int nthFibonacci){
        this.nthFibonacci = nthFibonacci;
    }

    public BigInteger claculateFibonacci(){
        BigInteger a = new BigInteger("0");
        BigInteger b = new BigInteger("1");
        while (nthFibonacci-- > 0) {
            BigInteger t = b;
            b = a.add(b);
            a = t;
        }

        return a;
    }

}
