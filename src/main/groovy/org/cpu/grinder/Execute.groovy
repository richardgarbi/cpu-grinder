package org.cpu.grinder

import java.util.stream.IntStream

/**
 * Created by Richard on 1/20/14.
 */
public class Execute {

    private static final int NUMBER_OF_THREADS = 80;

    public static void main(String[] args){

        List<Thread> allThreads = new ArrayList<Thread>();


        IntStream.range(0, NUMBER_OF_THREADS).forEach({
                Loader loadIt = new Loader(35000)
                Thread wrapper = new Thread(loadIt);
                wrapper.start();
                allThreads.add(wrapper);
        });

        boolean done = false;
        while(!done){

            for(Thread thread : allThreads){
                if(thread.getState() == Thread.State.TERMINATED){
                    allThreads.remove(thread);
                    break;
                }
            }

            if(allThreads.size() == 0){
                done = true;
            }

        }
    }

}
