package org.cpu.grinder

/**
 * Created by Richard on 1/20/14.
 */
public class Loader implements Runnable{

    private long lengthOfTime = 0;

    public Loader(long lengthOfTimeToRun){
         lengthOfTime = lengthOfTimeToRun
    }


    @Override
    void run() {

        println "Loadr up!"
        fib()

    }

    public void fib(){
        boolean keepGoing = true;

        long start = System.currentTimeMillis()


        while(keepGoing){
            int place = new Random().nextInt(200)

            BigInteger a = new BigInteger("0");
            BigInteger b = new BigInteger("1");
            while (place-- > 1) {
                BigInteger t = b;
                b = a.add(b);
                a = t;
            }

            if(System.currentTimeMillis() - start >= lengthOfTime){
                keepGoing = false;
            }
        }

        println "Done!"

    }

    public void test(){

    }
}


